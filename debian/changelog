python-pyramid-chameleon (0.3-8) unstable; urgency=medium

  * Team upload.
  * d/rules: Copy tests/fixture folder to builder folder to avoid
    fail tests (Closes: #1056499, #1052780).
  * d/control: Remove python3-nose as build dependency. Use python3-pytest
    instead (Closes: #1018567).
    - d/rules: Use pytest to run tests. Update d/tests/control to run with
    pytest. Ignore tests/test_localization.py and tests/test_renderers.py
    tests in autopkgtest.
  * d/control. Bump Standards-Version to 4.7.0 (from 4.6.1; no further changes
    needed).

 -- Emmanuel Arias <eamanu@debian.org>  Thu, 09 May 2024 21:17:14 -0300

python-pyramid-chameleon (0.3-7) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on dh-python.
  * Set upstream metadata fields: Bug-Submit.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 28 Oct 2022 19:49:01 +0100

python-pyramid-chameleon (0.3-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-pyramid-chameleon-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 11:20:59 +0100

python-pyramid-chameleon (0.3-5) unstable; urgency=medium

  * debian/patches/PR34.patch
    - fix compatibility with pyramid 2.x; Closes: #996406

 -- Sandro Tosi <morph@debian.org>  Wed, 01 Dec 2021 00:08:38 -0500

python-pyramid-chameleon (0.3-4) unstable; urgency=medium

  * Team Upload.
  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Repository, Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with
    new Debian Python Team contact address
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa layout

  [ Nilesh Patra ]
  * Add onovy's changes for 0.3-3 which seem unpushed
  * Fix doc build
  * Add python3-setuptools to Build-deps
    (Closes: #975814)
  * Bump Watch file standard to 4
  * Standards-Version: 4.5.1, debhelper-compat: 13
  * Add "Rules-Requires-Root:no"

 -- Nilesh Patra <npatra974@gmail.com>  Sun, 13 Dec 2020 19:27:55 +0000

python-pyramid-chameleon (0.3-3) unstable; urgency=medium

  * Team upload.
  * Disable intersphinx mapping (Closes: #915336)

 -- Ondřej Nový <onovy@debian.org>  Sat, 10 Aug 2019 11:59:34 +0200

python-pyramid-chameleon (0.3-2) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Set PYBUILD_NAME.
  * Disable failing test.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).
  * Enable autopkgtest-pkg-python testsuite instead of manual one.

 -- Ondřej Nový <onovy@debian.org>  Fri, 09 Aug 2019 11:12:38 +0200

python-pyramid-chameleon (0.3-1) unstable; urgency=medium

  * Initial release (Closes: #785048).
    - Also package Python 2 version as it is an enhancement to python-pyramid.

 -- Nicolas Dandrimont <olasd@debian.org>  Thu, 26 Apr 2018 16:44:59 +0200
